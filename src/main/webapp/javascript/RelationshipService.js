angular.module("Home").factory("RelationshipService", function($http,$q){
	return {
		getRelatives: function(personName, relationShip){
			var deferred = $q.defer();
			if(personName){
				$http.get('getRelatives?name=' + personName + "&relationShip=" + relationShip).
				  success(function(data, status, headers, config) {
					  if(data.result == 500){
						  deferred.reject(data);
					  }
					  else if(data.result == 501){
						  deferred.reject(data);
					  }
					  else{
					  deferred.resolve(data);
					  }
				  })
			}
			return deferred.promise;
		}
	}
})