var homeApp = angular.module("Home", []);

angular.module("Home").controller('HomeCtrl',['$scope','RelationshipService', function($scope, RelationshipService){
	$scope.relatives = [];
	$scope.availablePersons = [];

	
	
	 $scope.relations = [
	                  {name:'Brothers',id:'1'},
	                  {name:'Sisters',id:'2'},
	                  {name:'Mother',id:'3'},
	                  {name:'Father',id:'4'},
	                  {name:'Spouse',id:'5'},
	                  {name:'GrandFather',id:'6'},
	                  {name:'GrandMother',id:'7'},
	                  {name:'GrandSons',id:'8'},
	                  {name:'GrandDaughters',id:'9'},
	                  {name:'GrandChildren',id:'10'},
	                  {name:'Sons',id:'11'},
	                  {name:'Daughters',id:'12'},
	                  {name:'PaternalUncles',id:'13'},
	                  {name:'PaternalAunts',id:'14'},
	                  {name:'MaternalUncles',id:'15'},
	                  {name:'MaternalAunts',id:'16'},
	                  {name:'BrothersInLaw',id:'17'},
	                  {name:'SistersInLaw',id:'18'},
	                  {name:'FirstCousins',id:'19'}
	                ];
	 
	 $scope.$watch('relationName', function(newName){
		relation(newName);
		 
	 });
	 
	 $scope.go = function(){
		 relation($scope.relationName);
	 }
	 
	 function relation(newName){
		 
		 var promise
		 if(newName.name){
			 promise = RelationshipService.getRelatives($scope.name, newName.name);
			 promise.then(function(brothers){
				 $scope.relatives = brothers.result;
			 },
			 function(error){
				 if(error.result == 500){
					 alert("Person does not exist in Shan family.");
					 console.log(error);
				 }
				 if(error.result == 501){
					 alert(newName.name +""+ "is/are not present in the family tree for this person");
				 }
			 })
		 }
	 }
	 
}]);