<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="Home">
<head>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/custom.css">
<title>Meet The Family</title>
</head>
<body ng-controller="HomeCtrl" style="background-color: azure;">

	<nav class="navbar navbar-default">
	<div class="container-fluid header-background">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#" style="color:cornsilk;">Shan Dynasty</a>
		</div>
	</div>
	</nav>


		<div class="container">
			<div class="form-group row padding-left-right">
				<input class="custom-form-control col-md-4" name="name" id="name"
					type="text" placeholder="Enter a Name." ng-model="name" /> 
					<select
					class="custom-form-control col-md-4 col-md-offset-2"
					ng-model="relationName"
					ng-options="relation.name for relation in relations">
					<option value="">Choose A Relation</option>
				</select>
				<button type="button" name="search" id="search"
				class=" btn custom-btn-primary col-md-offset-1" ng-click="go()">
				<span class="glyphicon glyphicon-search"></span>
			</button>
			</div>

			<div class="form-group row padding-left-right">
				<div class="col-md-10 border" style="margin-bottom: 5px;" ng-repeat="person in relatives">
					<div class="col-md-6" style="padding: 10px;">{{person.name}}</div>
				</div>
			</div>
		</div>
</body>
<script src="javascript/angular.min.js"></script>
<script src="javascript/HomeCtrl.js"></script>
<script src="javascript/RelationshipService.js"></script>
</html>