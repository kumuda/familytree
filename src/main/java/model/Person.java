package model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.Query;
import org.hibernate.Session;

import exceptions.InvalidInputException;
import exceptions.InvalidPersonException;
import util.HibernateUtil;

@Entity
@Table(name = "persons")
public class Person {

	@Id
	@GeneratedValue
	@Column(name = "person_id")
	private int personId;

	@Column(name = "name", nullable = false)
	private String name;

	@Enumerated(EnumType.STRING)
	@Column(name = "gender")
	private Gender gender;

	public Person(String name, Gender gender) {

		this.name = name;
		this.gender = gender;
	}

	public Person() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public int getId() {
		return personId;
	}

	public void setId(int id) {
		this.personId = id;
	}

	@Override
	public String toString() {
		String person = null;
		person = "{ \"name\":" + "\"" + this.getName() + "\"," + "\"gender\":"
				+ "\"" + this.getGender() + "\",\"id\":" + "\"" + this.getId()
				+ "\"},";
		return person;
	}

	@Override
	public boolean equals(Object person) {
		if (person == null) {
			return false;
		}
		if (!(person instanceof Person)) {
			return false;
		}
		Person otherPerson = (Person) person;
		if (this.personId != otherPerson.getId()) {
			return false;
		}
		if (!this.name.equals(otherPerson.getName())) {
			return false;
		}
		if (!this.gender.equals(otherPerson.getGender())) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		return (int) this.name.hashCode() * this.gender.hashCode()
				* this.personId;
	}
	
	public static boolean checkIfPersonExists(String name) throws InvalidInputException, InvalidPersonException{
		if(name == null || name.equals("")){
			throw new InvalidInputException("Person name cannot be empty or null");
		}
		Session session = HibernateUtil.getSessionFactory().openSession();
		String hql = "select person from Person person where person.name= :name";
		Query query = session.createQuery(hql);
		query.setParameter("name", name);
		List result = query.list();
		if(result.isEmpty()){
			throw new InvalidPersonException("Person Does not exist");
		}
		
		return true;
	}
	
}
