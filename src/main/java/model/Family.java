package model;

import java.util.HashSet;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.Query;
import org.hibernate.Session;

import util.HibernateUtil;
import exceptions.InvalidInputException;
import exceptions.InvalidPersonException;

@Entity
@Table(name = "families")
public class Family {

	@Id
	@GeneratedValue
	@Column(name = "family_id")
	private int familyId;

	@OneToOne
	@JoinColumn(name = "wife")
	private Person wife;

	@OneToOne
	@JoinColumn(name = "husband")
	private Person husband;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "children_family_mapping", joinColumns = @JoinColumn(name = "f_id", referencedColumnName = "family_id"), inverseJoinColumns = @JoinColumn(name = "c_id", referencedColumnName = "person_id", insertable = false, updatable = false))
	private List<Person> children;

	@Column(name = "family_name")
	private String familyName;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "descendant_ancestor_family_mapping", joinColumns = @JoinColumn(name = "ancestor_family_id", referencedColumnName = "family_id"), inverseJoinColumns = @JoinColumn(name = "descendant_family_id", referencedColumnName = "family_id"))
	private List<Family> descendantFamilies;

	@OneToOne
	@JoinColumn(name = "ancestor")
	private Family ancestorFamily;

	public Family(Person husband, Person wife, List<Person> children) {

		this.husband = husband;
		this.wife = wife;
		this.children = children;
		this.familyName = husband.getName();

	}

	public Family() {

	}

	public List<Person> getChildren() {
		return children;
	}

	public void setChildren(List<Person> children) {
		this.children = children;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public List<Family> getDescendantFamilies() {
		return descendantFamilies;
	}

	public void setDescendantFamilies(List<Family> descendantFamilies) {
		this.descendantFamilies = descendantFamilies;
	}

	public int getFamilyId() {
		return familyId;
	}

	public void setFamilyId(int familyId) {
		this.familyId = familyId;
	}

	public Person getWife() {
		return wife;
	}

	public void setWife(Person wife) {
		this.wife = wife;
	}

	public Person getHusband() {
		return husband;
	}

	public void setHusband(Person husband) {
		this.husband = husband;
	}

	public Family getAncestorFamily() {
		return ancestorFamily;
	}

	public void setAncestorFamily(Family ancestorFamily) {
		this.ancestorFamily = ancestorFamily;
	}
	
	public static HashSet<Person> getSiblings(String name) throws InvalidInputException, InvalidPersonException{	
		checkIfPersonExists(name);
		String hql = "select family.children from Family family join family.children c where c.name= :name";
		return getPersons(name,hql);
	
	}
	
	public static HashSet<Person> getFather(String childName) throws InvalidPersonException, InvalidInputException{
		checkIfPersonExists(childName);
		String hql = "select family.husband from Family family join family.children c where c.name= :name";
		return getPersons(childName, hql);
	}
	
	public static HashSet<Person> getMother(String childName) throws InvalidPersonException, InvalidInputException{
		checkIfPersonExists(childName);
		String hql = "select family.wife from Family family join family.children c where c.name= :name";
		return getPersons(childName, hql);
	}
	
	public static HashSet<Person> getGrandFather(String childName) throws InvalidPersonException, InvalidInputException{
		checkIfPersonExists(childName);
		String hql = "select family.ancestorFamily.husband from Family family join family.children c where c.name= :name";
		return getPersons(childName, hql);
	}
	
	public static HashSet<Person> getGrandMother(String childName) throws InvalidPersonException, InvalidInputException{
		checkIfPersonExists(childName);
		String hql = "select family.ancestorFamily.wife from Family family join family.children c where c.name= :name";
		return getPersons(childName, hql);
	}
		
	public static Family getSpouse(String name) throws InvalidPersonException, InvalidInputException{
		checkIfPersonExists(name);
		String hql = "select family from Family family where (family.husband.name= :name or family.wife.name= :name)";
		return getFamily(name, hql);
	}
	
	public static HashSet<Person> getGrandChildren(String name) throws InvalidPersonException, InvalidInputException{
		checkIfPersonExists(name);
		String hql = "select family.children from Family family join family.children where (family.ancestorFamily.husband.name= :name or family.ancestorFamily.wife.name= :name)";
		return getPersons(name, hql);
	}
	
	
	public static HashSet<Person> getChildren(String name) throws InvalidPersonException, InvalidInputException{
		checkIfPersonExists(name);
		String hql = "select family.children from Family family where (family.husband.name= :name or family.wife.name= :name)";
		return getPersons(name, hql);
	}
	
	public static HashSet<Person> getUnclesAndAunts(String name) throws InvalidPersonException, InvalidInputException{
		checkIfPersonExists(name);
		String hql = "select family.ancestorFamily.children from Family family join family.children c where c.name= :name";
		return getPersons(name, hql);
	}
	
	private static HashSet<Person> getPersons(String name, String hql){	
		List result = getResultSet(name, hql);
		HashSet<Person> persons = new HashSet<Person>();
		if (result != null && !result.isEmpty()) {
			for (Object object : result) {
				Person person = (Person) object;
				persons.add(person);
			}
		}
		return persons;
	}
	
	private static Family getFamily(String name, String hql){
		List result = getResultSet(name, hql);
		Family family = null;
		if (result != null && !result.isEmpty()) {
			for (Object object : result) {
				family = (Family) object;
			}
		}
		return family;
	}
	
	private static List getResultSet(String name, String hql){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = session.createQuery(hql);
		query.setParameter("name", name);
		List result = query.list();
		if (!result.isEmpty()) {
			session.close();
			return result;
		}
		session.close();
		return null;
	}
	
	private static boolean checkIfPersonExists(String name) throws InvalidPersonException, InvalidInputException{
		boolean doesExist = false;
		doesExist = Person.checkIfPersonExists(name);
		return doesExist;
	}

}
