package exceptions;

public class InvalidPersonException extends Exception {

private String message;
	
	public InvalidPersonException(String message){
		this.message = message;
	}
	
	public String getMessage(){
		return message;
	}
}
