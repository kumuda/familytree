package controller;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;

import model.Person;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import service.RelationshipsService;


@Controller
public class RelationshipsController {

	@Autowired
	RelationshipsService relationshipService;


	@RequestMapping(value = "/getRelatives", method = RequestMethod.GET)
	public @ResponseBody
	String getRelatives(
			@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "relationShip", required = false) String relation) {
		JSONObject resultJson = new JSONObject();
		System.out.println("name:-" + name);
		String relationShip = Character.toUpperCase(relation.charAt(0)) + relation.substring(1);
		System.out.println("relationShip:  " + "get"+relationShip);
		HashSet<Person> result = new HashSet<Person>();
	
				Method methodToFind = null;
				
				try {
					methodToFind = RelationshipsService.class.getMethod("get"+relationShip,new Class[] { String.class });
					System.out.println(methodToFind.getName());
					result = (HashSet<Person>) methodToFind.invoke(relationshipService, name);
					resultJson.put("result", result);
					return resultJson.toString();
					
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
					resultJson.put("result", 501);
					return resultJson.toString();		
				} catch (SecurityException e) {
					e.printStackTrace();
					resultJson.put("result", 501);
					return resultJson.toString();		
				} catch (IllegalAccessException e) {
					resultJson.put("result", 501);
					return resultJson.toString();	
				} catch (IllegalArgumentException e) {
					resultJson.put("result", 501);
					return resultJson.toString();	
				} catch (InvocationTargetException e) {
					resultJson.put("result", 501);
					return resultJson.toString();	
				}	
	}

}
