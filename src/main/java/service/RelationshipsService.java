package service;

import java.util.HashSet;

import model.Family;
import model.Gender;
import model.Person;
import exceptions.InvalidInputException;
import exceptions.InvalidPersonException;

public class RelationshipsService {


	public boolean person(String name) throws InvalidInputException,
			InvalidPersonException {
		return Person.checkIfPersonExists(name);
	}

	private HashSet<Person> filterResultByGender(HashSet<Person> result,
			String name, Gender gender) {
		HashSet<Person> filteredResult = new HashSet<Person>();
		for (Person person : result) {
			if (person.getGender().equals(gender)
					&& !person.getName().equals(name)) {
				filteredResult.add(person);
			}
		}
		return filteredResult;
	}

	private HashSet<Person> getUncles(String name, HashSet<Person> parents)
			throws InvalidInputException, InvalidPersonException {
		HashSet<Person> uncles = new HashSet<Person>();
		Person parent = null;
		if (!parents.isEmpty()) {
			parent = parents.iterator().next();
		}
		HashSet<Person> unclesAndAunts = Family.getSiblings(parent.getName());
		for (Person person : unclesAndAunts) {
			if (person.getGender() == Gender.M && !parent.equals(person)) {
				uncles.add(person);
			} else if (person.getGender() == Gender.F && !parent.equals(person)) {
				Family auntsFamily = Family.getSpouse(person.getName());
				if (auntsFamily != null) {
					System.out.println(auntsFamily.getHusband());
					uncles.add(auntsFamily.getHusband());
				}
			}
		}
		return uncles;
	}

	private HashSet<Person> getAunts(String name, HashSet<Person> parents)
			throws InvalidInputException, InvalidPersonException {
		HashSet<Person> aunts = new HashSet<Person>();
		Person parent = null;
		if (!parents.isEmpty()) {
			parent = parents.iterator().next();
		}
		HashSet<Person> unclesAndAunts = Family.getSiblings(parent.getName());
		for (Person person : unclesAndAunts) {
			if (person.getGender() == Gender.F && !parent.equals(person)) {
				aunts.add(person);
			} else if (person.getGender() == Gender.M && !parent.equals(person)) {
				Family auntsFamily = Family.getSpouse(person.getName());
				if (auntsFamily != null) {
					System.out.println(auntsFamily.getWife());
					aunts.add(auntsFamily.getWife());
				}
			}
		}
		return aunts;

	}

	private Person getPersonFromSet(HashSet<Person> persons) {
		Person person = null;
		if (!persons.isEmpty()) {
			person = persons.iterator().next();
		}
		return person;
	}

	public HashSet<Person> getBrothers(String name)
			throws InvalidPersonException, InvalidInputException {

		HashSet<Person> siblings = Family.getSiblings(name);
		return filterResultByGender(siblings, name, Gender.M);
	}

	public HashSet<Person> getSisters(String name)
			throws InvalidPersonException, InvalidInputException {
		HashSet<Person> siblings = Family.getSiblings(name);
		return filterResultByGender(siblings, name, Gender.F);
	}

	public HashSet<Person> getFather(String childName)
			throws InvalidPersonException, InvalidInputException {
		return Family.getFather(childName);
	}

	public HashSet<Person> getMother(String childName)
			throws InvalidPersonException, InvalidInputException {
		return Family.getMother(childName);
	}

	public HashSet<Person> getGrandFather(String childName)
			throws InvalidPersonException, InvalidInputException {
		return Family.getGrandFather(childName);
	}

	public HashSet<Person> getGrandMother(String childName)
			throws InvalidPersonException, InvalidInputException {
		return Family.getGrandMother(childName);
	}

	public HashSet<Person> getSpouse(String name)
			throws InvalidPersonException, InvalidInputException {
		Family family = Family.getSpouse(name);
		HashSet<Person> spouse = new HashSet<Person>();
		if (family != null) {
			if (name.equals(family.getHusband().getName())) {
				spouse.add(family.getWife());
			} else {
				spouse.add(family.getHusband());
			}
		}
		return spouse;
	}

	public HashSet<Person> getGrandChildren(String name)
			throws InvalidPersonException, InvalidInputException {
		return Family.getGrandChildren(name);
	}

	public HashSet<Person> getGrandSons(String name)
			throws InvalidPersonException, InvalidInputException {
		HashSet<Person> grandChildren = this.getGrandChildren(name);
		return this.filterResultByGender(grandChildren, name, Gender.M);

	}

	public HashSet<Person> getGrandDaughters(String name)
			throws InvalidPersonException, InvalidInputException {
		HashSet<Person> grandChildren = this.getGrandChildren(name);
		return this.filterResultByGender(grandChildren, name, Gender.F);

	}

	public HashSet<Person> getSons(String name) throws InvalidPersonException,
			InvalidInputException {
		HashSet<Person> children = Family.getChildren(name);
		return this.filterResultByGender(children, name, Gender.M);
	}

	public HashSet<Person> getDaughters(String name)
			throws InvalidPersonException, InvalidInputException {
		HashSet<Person> children = Family.getChildren(name);
		return this.filterResultByGender(children, name, Gender.F);
	}

	public HashSet<Person> getPaternalUncles(String name)
			throws InvalidPersonException, InvalidInputException {
		HashSet<Person> father = Family.getFather(name);
		return this.getUncles(name, father);
	}

	public HashSet<Person> getPaternalAunts(String name)
			throws InvalidPersonException, InvalidInputException {
		HashSet<Person> father = Family.getFather(name);
		return this.getAunts(name, father);
	}

	public HashSet<Person> getMaternalUncles(String name)
			throws InvalidPersonException, InvalidInputException {
		HashSet<Person> mother = Family.getMother(name);
		return this.getUncles(name, mother);
	}

	public HashSet<Person> getMaternalAunts(String name)
			throws InvalidPersonException, InvalidInputException {
		HashSet<Person> mother = Family.getMother(name);
		return this.getAunts(name, mother);
	}

	public HashSet<Person> getFirstCousins(String name)
			throws InvalidPersonException, InvalidInputException {
		HashSet<Person> cousins = new HashSet<Person>();
		HashSet<Person> grandFathers = Family.getGrandFather(name);
		Person grandFather = this.getPersonFromSet(grandFathers);
		if (grandFather != null) {
			HashSet<Person> grandChildren = Family.getGrandChildren(grandFather
					.getName());
			HashSet<Person> siblings = Family.getSiblings(name);
			for (Person person : grandChildren) {
				if (!siblings.contains(person)) {
					cousins.add(person);
				}
			}
		}
		return cousins;
	}

	public HashSet<Person> getBrothersInLaw(String name)
			throws InvalidPersonException, InvalidInputException {
		HashSet<Person> brothersInLaw = new HashSet<Person>();
		HashSet<Person> result = this.getSpouse(name);
		Person spouse = this.getPersonFromSet(result);
		if (spouse != null) {
			HashSet<Person> spouseBrothers = this.getBrothers(spouse.getName());
			brothersInLaw.addAll(spouseBrothers);
		}
		HashSet<Person> sisters = this.getSisters(name);
		for (Person person : sisters) {
			spouse = this.getPersonFromSet(this.getSpouse(person.getName()));
			if (spouse != null) {
				brothersInLaw.add(spouse);
			}
		}
		return brothersInLaw;
	}

	public HashSet<Person> getSistersInLaw(String name)
			throws InvalidPersonException, InvalidInputException {
		HashSet<Person> sistersInLaw = new HashSet<Person>();
		HashSet<Person> result = this.getSpouse(name);
		Person spouse = this.getPersonFromSet(result);
		if (spouse != null) {
			HashSet<Person> spouseSisters = this.getSisters(spouse.getName());
			sistersInLaw.addAll(spouseSisters);
		}
		HashSet<Person> brothers = this.getBrothers(name);
		for (Person person : brothers) {
			spouse = this.getPersonFromSet(this.getSpouse(person.getName()));
			if (spouse != null) {
				sistersInLaw.add(spouse);
			}
		}
		return sistersInLaw;
	}
}