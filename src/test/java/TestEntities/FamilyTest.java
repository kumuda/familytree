package TestEntities;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;

import model.Family;
import model.Gender;
import model.Person;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import exceptions.InvalidInputException;
import exceptions.InvalidPersonException;

public class FamilyTest {
	
	@Before
	public void setUp(){
		
	}
	
	@After
	public void tearDown(){
		
	}
	
	@Test
	public void checkGetSiblingsWhenPresent() throws InvalidInputException, InvalidPersonException{
		HashSet<Person> siblings = Family.getSiblings("Chit");
		assertEquals(4, siblings.size());
	}
	
	@Test
	public void checkGetSiblingsWhenNotPresent() throws InvalidInputException, InvalidPersonException{
		HashSet<Person> siblings = Family.getSiblings("Shan");
		assertEquals(0, siblings.size());
	}
	
	@Test
	public void checkGetSiblingsWhenPersonNotPresent(){
		try{
			Family.getSiblings("Kumuda");
			assert false;
		}
		catch(InvalidPersonException e){
			assert true;
		}
		catch(InvalidInputException e){
			assert false;
		}
	}
	
	@Test
	public void checkGetSiblingsWhenArgumentNull(){
		try{
			Family.getSiblings(null);
			assert false;
		}
		catch(InvalidPersonException e){
			assert false;
		}
		catch(InvalidInputException e){
			assert true;
		}
	}
	
	@Test
	public void checkGetSiblingsWhenArgumentEmpty(){
		try{
			Family.getSiblings("");
			assert false;
		}
		catch(InvalidPersonException e){
			assert false;
		}
		catch(InvalidInputException e){
			assert true;
		}
	}
	
	@Test
	public void checkGetFatherWhenPresent() throws InvalidPersonException, InvalidInputException{
		HashSet<Person> result = Family.getFather("Chit");
		Person father = this.getPersonFromSet(result);
		assertEquals("shan", father.getName());		
	}
	
	@Test
	public void checkGetFatherWhenNotPresent() throws InvalidPersonException, InvalidInputException{
		HashSet<Person> result = Family.getFather("Shan");
		assertEquals(0, result.size());
	}
	
	
	@Test
	public void checkGetFatherWhenPersonNotPresent(){
		try{
			Family.getFather("Kumuda");
			assert false;
		}
		catch(InvalidPersonException e){
			assert true;
		}
		catch(InvalidInputException e){
			assert false;
		}
	}
	
	@Test
	public void checkGetFatherWhenArgumentNull(){
		try{
			Family.getFather(null);
			assert false;
		}
		catch(InvalidPersonException e){
			assert false;
		}
		catch(InvalidInputException e){
			assert true;
		}
	}
	
	@Test
	public void checkGetFatherWhenArgumentEmpty(){
		try{
			Family.getFather("");
			assert false;
		}
		catch(InvalidPersonException e){
			assert false;
		}
		catch(InvalidInputException e){
			assert true;
		}
	}
	
	@Test
	public void checkGetMotherWhenPresent() throws InvalidPersonException, InvalidInputException{
		HashSet<Person> result = Family.getMother("Chit");
		Person mother = this.getPersonFromSet(result);
		assertEquals("Anga", mother.getName());		
	}
	
	@Test
	public void checkGetMotherWhenNotPresent() throws InvalidPersonException, InvalidInputException{
		HashSet<Person> result = Family.getMother("Shan");
		assertEquals(0, result.size());
	}
	
	
	@Test
	public void checkGetMotherWhenPersonNotPresent(){
		try{
			Family.getMother("Kumuda");
			assert false;
		}
		catch(InvalidPersonException e){
			assert true;
		}
		catch(InvalidInputException e){
			assert false;
		}
	}
	
	@Test
	public void checkGetMotherWhenArgumentNull(){
		try{
			Family.getMother(null);
			assert false;
		}
		catch(InvalidPersonException e){
			assert false;
		}
		catch(InvalidInputException e){
			assert true;
		}
	}
	
	@Test
	public void checkGetMotherWhenArgumentEmpty(){
		try{
			Family.getMother("");
			assert false;
		}
		catch(InvalidPersonException e){
			assert false;
		}
		catch(InvalidInputException e){
			assert true;
		}
	}
	
	@Test
	public void checkGetGrandFatherWhenPresent() throws InvalidPersonException, InvalidInputException{
		HashSet<Person> result = Family.getGrandFather("Drita");
		Person father = this.getPersonFromSet(result);
		assertEquals("shan", father.getName());		
	}
	
	@Test
	public void checkGetGrandFatherWhenNotPresent() throws InvalidPersonException, InvalidInputException{
		HashSet<Person> result = Family.getGrandFather("Shan");
		assertEquals(0, result.size());
	}
	
	
	@Test
	public void checkGetGrandFatherWhenPersonNotPresent(){
		try{
			Family.getGrandFather("Kumuda");
			assert false;
		}
		catch(InvalidPersonException e){
			assert true;
		}
		catch(InvalidInputException e){
			assert false;
		}
	}
	
	@Test
	public void checkGetGrandFatherWhenArgumentNull(){
		try{
			Family.getGrandFather(null);
			assert false;
		}
		catch(InvalidPersonException e){
			assert false;
		}
		catch(InvalidInputException e){
			assert true;
		}
	}
	
	@Test
	public void checkGetGrandFatherWhenArgumentEmpty(){
		try{
			Family.getGrandFather("");
			assert false;
		}
		catch(InvalidPersonException e){
			assert false;
		}
		catch(InvalidInputException e){
			assert true;
		}
	}
	
	@Test
	public void checkGetGrandMotherWhenPresent() throws InvalidPersonException, InvalidInputException{
		HashSet<Person> result = Family.getGrandMother("Drita");
		Person father = this.getPersonFromSet(result);
		assertEquals("Anga", father.getName());		
	}
	
	@Test
	public void checkGetGrandMotherWhenNotPresent() throws InvalidPersonException, InvalidInputException{
		HashSet<Person> result = Family.getGrandMother("Shan");
		assertEquals(0, result.size());
	}
	
	
	@Test
	public void checkGetGrandMotherWhenPersonNotPresent(){
		try{
			Family.getGrandMother("Kumuda");
			assert false;
		}
		catch(InvalidPersonException e){
			assert true;
		}
		catch(InvalidInputException e){
			assert false;
		}
	}
	
	@Test
	public void checkGetGrandMotherWhenArgumentNull(){
		try{
			Family.getGrandMother(null);
			assert false;
		}
		catch(InvalidPersonException e){
			assert false;
		}
		catch(InvalidInputException e){
			assert true;
		}
	}
	
	@Test
	public void checkGetGrandMotherWhenArgumentEmpty(){
		try{
			Family.getGrandMother("");
			assert false;
		}
		catch(InvalidPersonException e){
			assert false;
		}
		catch(InvalidInputException e){
			assert true;
		}
	}
	
	@Test
	public void checkGetSpouseWhenPresent() throws InvalidPersonException, InvalidInputException{
		Family family = Family.getSpouse("Shan");
		if(family != null){
			Person wife = family.getWife();
			assertEquals("Anga",wife.getName());
		}
		else{
			assert false;
		}
	}
	
	@Test
	public void checkGetSpouseWhenNotPresent() throws InvalidPersonException, InvalidInputException{
		Family family = Family.getSpouse("Ish");
		assertEquals(null, family);
	}
	
	@Test
	public void checkGetSpouseWhenPersonNotPresent(){
		try{
			Family family = Family.getSpouse("kumuda");
			assert false;
		}
		catch(InvalidPersonException e){
			assert true;
		}
		catch(InvalidInputException e){
			assert false;
		}
	}
	
	@Test
	public void checkGetSpouseWhenArgumentNull(){
		try{
			Family family = Family.getSpouse(null);
			assert false;
		}
		catch(InvalidPersonException e){
			assert false;
		}
		catch(InvalidInputException e){
			assert true;
		}
	}
	
	@Test
	public void checkGetSpouseWhenArgumentEmpty(){
		try{
			Family family = Family.getSpouse("");
			assert false;
		}
		catch(InvalidPersonException e){
			assert false;
		}
		catch(InvalidInputException e){
			assert true;
		}
	}
	
	
	@Test
	public void checkGetGrandChildrenWhenPresent() throws InvalidPersonException, InvalidInputException{
		HashSet<Person> result = Family.getGrandChildren("Chit");
		Person jata = new Person("Jata", Gender.M);	
		Person driya = new Person("Driya", Gender.F);
		assertEquals(2, result.size());
		if(result.contains(jata) && result.contains(driya)){
			assert true;
		}
	}
	
	@Test
	public void checkGetGrandChildrenWhenNotPresent() throws InvalidPersonException, InvalidInputException{
		HashSet<Person> result = Family.getGrandChildren("Ish");
		assertEquals(0, result.size());
	}
	
	
	@Test
	public void checkGetGrandChildrenWhenPersonNotPresent(){
		try{
			Family.getGrandChildren("Kumuda");
			assert false;
		}
		catch(InvalidPersonException e){
			assert true;
		}
		catch(InvalidInputException e){
			assert false;
		}
	}
	
	@Test
	public void checkGetGrandChildrenWhenArgumentNull(){
		try{
			Family.getGrandChildren(null);
			assert false;
		}
		catch(InvalidPersonException e){
			assert false;
		}
		catch(InvalidInputException e){
			assert true;
		}
	}
	
	@Test
	public void checkGetGrandChildrenWhenArgumentEmpty(){
		try{
			Family.getGrandChildren("");
			assert false;
		}
		catch(InvalidPersonException e){
			assert false;
		}
		catch(InvalidInputException e){
			assert true;
		}
	}
	
	
	@Test
	public void checkGetChildrenWhenPresent() throws InvalidPersonException, InvalidInputException{
		HashSet<Person> children = Family.getChildren("Vich");
		Person vila = new Person("Vila", Gender.M);	
		Person chika = new Person("Chika", Gender.F);
		assertEquals(2, children.size());
		if(children.contains(vila) && children.contains(chika)){
			assert true;
		}
		
	}
	
	@Test
	public void checkGetChildrenWhenNotPresent()throws InvalidInputException, InvalidPersonException {
		HashSet<Person> children = Family.getChildren("Satvy");
		assertEquals(0, children.size());
	}
	
	@Test
	public void checkGetChildrenWhenPersonNotPresent(){
		try{
			Family.getChildren("Kumuda");
			assert false;
		}
		catch(InvalidPersonException e){
			assert true;
		}
		catch(InvalidInputException e){
			assert false;
		}
	}
	@Test
	public void checkGetChildrenWhenArgumentNull(){
		try{
			Family.getChildren(null);
			assert false;
		}
		catch(InvalidPersonException e){
			assert false;
		}
		catch(InvalidInputException e){
			assert true;
		}
	}
	
	@Test
	public void checkGetChildrenWhenArgumentEmpty(){
		try{
			Family.getChildren("");
			assert false;
		}
		catch(InvalidPersonException e){
			assert false;
		}
		catch(InvalidInputException e){
			assert true;
		}
	}
	
	@Test
	public void checkGetUnclesAndAuntsWhenPresent() throws InvalidPersonException, InvalidInputException{
		HashSet<Person> unclesAndAunts = Family.getUnclesAndAunts("Satvy");
		assertEquals(4, unclesAndAunts.size());
	}
	
	@Test
	public void checkGetUnclesAndAuntsWhenNotPresent() throws InvalidPersonException, InvalidInputException{
		HashSet<Person> unclesAndAunts = Family.getUnclesAndAunts("Satya");
		assertEquals(0, unclesAndAunts.size());
	}
	
	@Test
	public void checkGetUnclesAndAuntsWhenPersonNotPresent(){
		try{
			Family.getUnclesAndAunts("kumuda");
			assert false;
		}
		catch(InvalidPersonException e){
			assert true;
		}
		catch(InvalidInputException e){
			assert false;
		}
	}
	
	@Test
	public void checkGetUnclesAndAuntsArgumentNull(){
		try{
			Family.getUnclesAndAunts(null);
			assert false;
		}
		catch(InvalidPersonException e){
			assert false;
		}
		catch(InvalidInputException e){
			assert true;
		}
	}
	
	@Test
	public void checkGetUnclesAndAuntsArgumentEmpty(){
		try{
			Family.getUnclesAndAunts("");
			assert false;
		}
		catch(InvalidPersonException e){
			assert false;
		}
		catch(InvalidInputException e){
			assert true;
		}
	}	
	
	private Person getPersonFromSet(HashSet<Person> persons){
		Person person = null;
		if(!persons.isEmpty()){
			person = persons.iterator().next();
		}	
		return person;
	}

}
