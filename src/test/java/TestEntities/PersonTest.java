package TestEntities;

import static org.junit.Assert.assertEquals;
import model.Person;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import exceptions.InvalidInputException;
import exceptions.InvalidPersonException;

public class PersonTest {

	@Before
	public void setUp() {

	}

	@After
	public void tearDown() {

	}

	@Test
	public void checkGetPersonWhenPersonPresent() throws InvalidInputException,
			InvalidPersonException {
		boolean personExists = false;
		personExists = Person.checkIfPersonExists("shan");
		assertEquals(true, personExists);
	}

	@Test
	public void checkGetPersonWhenPersonNotPresent() {
		try {
			Person.checkIfPersonExists("kumuda");
			assert false;
		} catch (InvalidPersonException e) {
			assert true;
		} catch (InvalidInputException e) {
			assert false;
		}

	}

	@Test
	public void checkGetPersonWhenArgumentIsNull() {
		try {
			Person.checkIfPersonExists(null);
			assert false;
		} catch (InvalidInputException e) {
			assert true;
		} catch (InvalidPersonException e) {
			assert false;
		}
	}

	@Test
	public void checkGetPersonWhenArgumentIsEmpty() {
		try {
			Person.checkIfPersonExists(null);
			assert false;
		} catch (InvalidInputException e) {
			assert true;
		} catch (InvalidPersonException e) {
			assert false;
		}
	}

}
