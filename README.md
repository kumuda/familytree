This is a family tree project.  

Given a name and relation, if the family details of that person exists, results are shown.  

To run the application,   
You need Java7, Maven, apache-tomcat-7*  

Step1: Go to the project folder and execute below command:    
       mvn clean package   
Step2: Once the build is successfull, Go into the target folder present inside familytree folder, copy the war file and  paste it in /path/to/apache-tomcat/webapps/familytree.war

step3: Go to bin folder present in apache-tomcat and run the startup.sh file.